Better Maintenance Mode for Drupal 7.x
----------------------------
This module provides a second option for maintenance mode in Drupal. The
standard maintenance mode hides the entire site behind the maintenance message.
This module allows the whole site to be visible, however it blocks any editing
the site. This allows for a clean migration of a site between servers, with
minimal visible downtime.

Installation
------------
Better Maintenance Mode can be installed like any other Drupal module -- place
it in the modules directory for your site and enable it on the
'admin/build/modules' page. See
https://www.drupal.org/documentation/install/modules-themes/modules-7 for more
details.

Using Commerce Postmaster
-------------------
Similar to maintenance mode, simple go to Configuration -> Development ->
Maintenance Mode -> Better Maintenance Mode. Check the Put site into better
maintenance mode check box, enter your maitnenance mode message and click Save
Configuration. When ever a user attempts to save an entity or add a submission
to a Webform, they will be redirected to the Maintenance Mode page, displaying
the maintenance message.

Maintainers
-----------

- kafmil (Kurt Foster)
