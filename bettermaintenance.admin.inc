<?php

/**
 * @file
 * Contains the admin form for the better maintenance module.
 */

/**
 * Function bettermaintenance_admin_form.
 *
 * Admin settings form bettermaintenance_admin_form.
 *
 * @return array
 *   System settings form.
 */
function bettermaintenance_admin_form() {
  $form = array();

  $form['bettermaintenance_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Put site into better maintenance mode'),
    '#default_value' => variable_get('bettermaintenance_mode', 0),
    '#description' => t('When enabled, only users with the "Use the site in better maintenance mode" <a href="@permissions-url">permission</a> are able to access your site to perform maintenance; all other visitors see the maintenance mode message configured below. Authorized users can log in directly via the <a href="@user-login">user login</a> page.', array('@permissions-url' => url('admin/people/permissions'), '@user-login' => url('user'))),
  );
  $form['bettermaintenance_mode_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Maintenance mode message'),
    '#default_value' => variable_get('bettermaintenance_mode_message', t('@site is currently under maintenance. We should be back shortly. Thank you for your patience.', array('@site' => variable_get('site_name', 'Drupal')))),
    '#description' => t('Message to show visitors when they try to edit the site.'),
  );

  return system_settings_form($form);
}
