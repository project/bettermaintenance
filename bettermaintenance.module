<?php

/**
 * @file
 * Hook implementations and shared functions.
 */

/**
 * Implements hook_menu().
 */
function bettermaintenance_menu() {
  $items['admin/config/development/maintenance/bettermaintenance'] = array(
    'title' => 'Better Maintenance Mode',
    'description' => 'Put the site into better maintenance mode',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bettermaintenance_admin_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'bettermaintenance.admin.inc',
    'type' => MENU_NORMAL_ITEM,
    'weight' => 20,
  );

  $items['bettermaintenance'] = array(
    'title' => 'Maintenance Mode',
    'description' => 'The site is currently in maintenance mode',
    'page callback' => 'bettermaintenance_errorpage',
    'access arguments' => array('access content'),
    'file' => 'bettermaintenance.admin.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function bettermaintenance_permission() {
  return array(
    'access site in better maintenance mode' => array(
      'title' => t('Use the site in better maintenance mode'),
    ),
  );
}

/**
 * Implements hook_entity_presave().
 */
function bettermaintenance_entity_presave() {
  if (variable_get('bettermaintenance_mode', 0)) {
    unset($_GET['destination']);
    drupal_static_reset('drupal_get_destination');
    drupal_goto('bettermaintenance');
  }
}

/**
 * Implements hook_webform_submission_presave().
 */
function bettermaintenance_webform_submission_presave($node, &$submission) {
  if (variable_get('bettermaintenance_mode', 0)) {
    unset($_GET['destination']);
    drupal_static_reset('drupal_get_destination');
    drupal_goto('bettermaintenance');
  }
}

/**
 * Implements hook_theme().
 */
function bettermaintenance_theme() {
  return array(
    'bettermaintenance_errorpage' => array(
      'template' => 'bettermaintenance_errorpage',
      'variables' => array('bettermaintenance_errorpage' => NULL),
    ),
  );
}

/**
 * Function bettermaintenance_errorpage.
 *
 * Displays the better maintenance mode error message
 */
function bettermaintenance_errorpage() {
  if (variable_get('bettermaintenance_mode', 0)) {
    return theme('bettermaintenance_errorpage', array(
      'bettermaintenance_errorpage' => variable_get('bettermaintenance_mode_message', t('@site is currently under maintenance. We should be back shortly. Thank you for your patience.', array(
          '@site' => variable_get('site_name', 'Drupal'),
          ))),
      ));
  }
  else {
    drupal_goto('<front>');
  }
}
